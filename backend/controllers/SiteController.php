<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\Article;
use common\models\Category;
use yii\web\UploadedFile;
use common\models\File;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout','index', 'video','add','delete','file','add-file'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['login', 'error','role'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $articles = Article::find()->orderBy( 'id DESC')->all();

        return $this->render('index', [
            'articles' => $articles,
        ]);
    }

    public function actionDelete($id = false)
    {
        if (isset($id)) {
            if (Article::deleteAll(['in', 'id', $id])) {
                $this->redirect(['index']);
            }
        } else {
            $this->redirect(['index']);
        }
    }



    public function actionVideo($id)
    {

        $files = File::find()->where(['article_id' => $id])->all();
  
        $category = Category::find()->all();

        $article = Article::findOne($id);

        if($article->load(Yii::$app->request->post())){
            if($article->save())
                return $this->redirect(['site/index']);
                
      }

        return $this->render('video', [
            'article' => $article,
            'category' => $category,
            'files' => $files,
        ]);
    }

    public function actionFile($id){

      $files = File::find()->where(['article_id' => $id])->all();

      $article = File::find()->where(['id' => $id])->one();


      return $this->render('file',[
        'files' => $files,
        'article' => $article,
      ]);
    }

    public function actionAddFile($id){
      
        $model = new File();

        $model->article_id = $id;

        $model->date = date('Y-m-d');

        if($model->load(Yii::$app->request->post())){
          if($model->save()){
            return $this->redirect(['site/file','id'=> $id]);
          }
        }

      return $this->render('add-file',[
        'model' => $model,
      ]);
    }

    public function actionAdd(){

      
      $category = Category::find()->all();

      $model = new Article();
      $model->date = date('Y-m-d');
      $model->viewed = 0;
      if($model->load(Yii::$app->request->post())){
         if($model->validate()){
            $file = UploadedFile::getInstance($model,'image');
              if(file_exists($file)){
                $model->saveImage($this->uploadFile($file));
              }else{
                $model->image = NULL;
              }

              $icon = UploadedFile::getInstance($model,'image_icon');

              if(file_exists($icon)){
                $model->saveImageIcon($this->uploadFileIcon($icon));
              }else{
                $model->image_icon = NULL;
              }
              
              if($model->save())
                  return $this->redirect(['site/index']);
              }
      }
      return $this->render('add',[
        'model' => $model,
        'category' => $category,
      ]);
    }

    public function uploadFile(UploadedFile $file){
            
            $filename = md5(uniqid($file->baseName)).'.'.$file->extension;

            $file->saveAs(Yii::getAlias('@web').'img/'.$filename);

            return $filename;
    }

    public function uploadFileIcon(UploadedFile $icon){

            $fileicon = md5(uniqid($icon->baseName)).'.'.$icon->extension;

            $icon->saveAs(Yii::getAlias('@web').'images/'.$fileicon);

            return $fileicon;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRole(){

 /*     $admin = Yii::$app->authManager->createRole('admin');
      $admin->description = 'Администратор';
      Yii::$app->authManager->add($admin);
  
 
      $permit = Yii::$app->authManager->createPermission('createPost');
      $permit->description = 'Право входа в админку';
      Yii::$app->authManager->add($permit);
*/
      return 1232132;

    }

}
