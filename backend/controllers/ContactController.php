<?php

namespace backend\controllers;


use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Contact;

class ContactController extends Controller{

	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   [
                        'actions' => ['index','delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){

        $contacts = Contact::find()->orderBy('id DESC')->all();
    	

    	return $this->render('index',[
            'contacts' => $contacts,
        ]);
    }

    public function actionDelete($id = false)
    {
        if (isset($id)) {
            if (Contact::deleteAll(['in', 'id', $id])) {
                $this->redirect(['index']);
            }
        } else {
            $this->redirect(['index']);
        }
    }
}