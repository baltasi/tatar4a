<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
       
        <div class="container" style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
            <!-- row -->
            <div class="row tm-content-row" style="height: 100%; overflow-y: scroll;">
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll" style="height: 100%;">
                        <h2 class="tm-block-title">Фикерләр:</h2>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Текст</th>
                                    <th scope="col">Фильм</th>
                                    <th scope="col">Кулланучы</th>
                                    <th scope="col">Вакыты</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($comments as $comment):?>
                                <tr>
                                    <th scope="row"><b>#<?= $comment->id ?></b></th>
                                    <td><b><?= $comment->text ?></b></td>
                                    <td><b><?= $comment->article->title ?></b></td>
                                    <td><b><?= $comment->user->username ?></b></td>
                                    <td><b><?= $comment->date ?></b></td>
                                    <td>
                                        <?php if($comment->status === 1): ?>
                                            <div class="tm-status-circle moving">
                                        <?php else: ?>
                                            <div class="tm-status-circle cancelled">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= Url::to(['comment/delete', 'id' => $comment->id]) ?>" onclick="return confirm('Вы уверены?')" class="tm-product-delete-link">
                                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                                        </a>
                                    </td>
                                </tr>   
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>