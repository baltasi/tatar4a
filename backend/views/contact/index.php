<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
       
        <div class="container" style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
            <!-- row -->
            <div class="row tm-content-row" style="height: 100%; overflow-y: scroll;">
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll" style="height: 100%;">
                        <h2 class="tm-block-title">Хатлар:</h2>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Тема</th>
                                    <th scope="col">Текст</th>
                                    <th scope="col">Исеме</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($contacts as $contact):?>
                                <tr>
                                    <th scope="row"><b>#<?= $contact->id ?></b></th>
                                    <td><b><?= $contact->subject ?></b></td>
                                    <td><b><?= $contact->body ?></b></td>
                                    <td><b><?= $contact->name ?></b></td>
                                    <td><b><?= $contact->email ?></b></td>
                                    <td>
                                        <a href="<?= Url::to(['contact/delete', 'id' => $contact->id]) ?>" onclick="return confirm('Вы уверены?')" class="tm-product-delete-link">
                                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                                        </a>
                                    </td>
                                </tr>   
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>