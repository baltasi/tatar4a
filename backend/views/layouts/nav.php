<?php 
use yii\helpers\Html;

?>
<?php if (Yii::$app->user->can('admin')): ?>
<nav class="navbar navbar-expand-xl" style="width: 100%;z-index: 5;     height: auto;">
            <div class="container h-100">
                <a class="navbar-brand" href="/">
                    <h1 class="tm-site-title mb-0">Tatar4a.ru</h1>
                </a>
                <button class="navbar-toggler ml-auto mr-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars tm-nav-icon"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto h-100">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/users">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/comments">Comments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact">Contact</a>
                        </li>
                    </ul>
                    <?php   if(!Yii::$app->user->isGuest): ?>
                    <ul class="navbar-nav">
                        <?= Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Чыгу (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'nav-item']
                        )
                        . Html::endForm() ?>
                        
                    </ul>
                <?php endif ?>
                </div>
            </div>
        </nav>
<?php endif; ?>