 <?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Article;


 ?>
 <div class="container tm-mt-big tm-mb-big" style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
      <div class="row" style="height: 100%;  overflow-y: scroll;">
        <div class="tm-bg-primary-dark tm-block tm-block-h-auto" >
            <div class="row tm-edit-product-row" style="height: 100%;">
              <div class="col-xl-6 col-lg-6 col-md-12">
                <?php $form = ActiveForm::begin(['options' => ['class' => 'tm-edit-product-form']]); ?>
                  <div class="form-group mb-3">
                    <?= $form->field( $article, 'title')->textInput( ['class' => "form-control validate", 'value'=> ''. $article->title.'']) ?>
                  </div>
                  <div class="form-group mb-3">
                    <?= $form->field( $article, 'text')->textarea( ['class' => "form-control validate tm-small", 'rows'=> 5, 'value'=> ''. $article->text.'' ]) ?>
                  </div>
                  <div class="form-group mb-3">
                    <?= $form->field( $article, 'category' )->dropDownList(ArrayHelper::map($category,'id','title'),['class' => 'custom-select tm-select-accounts']); ?>
                    
                  </div>
                  <div class="row">
                      <div class="form-group mb-3 col-xs-12 col-sm-6">
                          <?= $form->field( $article, 'status' )->dropDownList(Article::getStatusList(),['class' => 'custom-select tm-select-accounts']); ?>
                        </div>
                        <div class="form-group mb-3 col-xs-12 col-sm-6">
                          <?= $form->field( $article, 'new' )->dropDownList(Article::getNewList(),['class' => 'custom-select tm-select-accounts']); ?>
                        </div>
                      </div>

                  <div class="row">
                      <div class="form-group mb-3 col-xs-12 col-sm-6">
                          <?= $form->field( $article, 'film_date')->textInput( ['class' => "form-control validate", 'value'=> "$article->film_date"]) ?>
                        </div>
                        <div class="form-group mb-3 col-xs-12 col-sm-6">
                          <?= $form->field( $article, 'viewed')->textInput( ['class' => "form-control validate", 'value'=> "$article->viewed"]) ?>
                        </div>
                  </div>
                  
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 mx-auto mb-4">

                <div class="custom-file mt-3 mb-3" style="height: calc(2.25rem + 25px);">
                  <?= Html::a('Файллар ('. count($files) .')', ['file','id' => $article->id],['class' => 'btn btn-primary btn-block mx-auto']) ?> 
                </div>
                <div class="tm-product-img-edit mx-auto">
                  <img src="/img/product-image.jpg" alt="Product image" class="img-fluid d-block mx-auto">
                  <i class="fas fa-cloud-upload-alt tm-upload-icon" onclick="document.getElementById('fileInput').click();" ></i>
                </div>
                <div class="custom-file mt-3 mb-3" style="height: calc(2.25rem + 25px);">
                  <input id="fileInput" type="file" style="display:none;" />
                  <input type="button" class="btn btn-primary btn-block mx-auto" value="CHANGE IMAGE NOW" onclick="document.getElementById('fileInput').click();"/>
                </div>

                 <div class="tm-product-img-edit mx-auto">
                  <img src="/img/product-image.jpg" alt="Product image" class="img-fluid d-block mx-auto">
                  <i class="fas fa-cloud-upload-alt tm-upload-icon" onclick="document.getElementById('fileInput').click();" ></i>
                </div>
               
                <div class="custom-file mt-3 mb-3">
                  <input id="fileInput" type="file" style="display:none;" />
                  <input type="button" class="btn btn-primary btn-block mx-auto" value="CHANGE IMAGE NOW" onclick="document.getElementById('fileInput').click();"/>
                </div>
              </div>
          
              <div class="col-12">
                <?= Html::submitButton( 'Сакларга', ['class' => 'btn btn-primary btn-block text-uppercase'] ) ?> 
              </div>
             <?php $form = ActiveForm::end(); ?>
            </div>
          </div>
      </div>
    </div>

