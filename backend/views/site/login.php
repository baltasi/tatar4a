<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="container tm-mt-big tm-mb-big">
      <div class="row">
        <div class="col-12 mx-auto tm-login-col">
          <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
            <div class="row">
              <div class="col-12 text-center">
                <h2 class="tm-block-title mb-4">Welcome!</h2>
              </div>
            </div>
            <div class="row mt-2">
              <div class="col-12">
                <?php $form = ActiveForm::begin(['id' => 'login-form'],['options' => ['class' => 'tm-login-form']]); ?>
                  <div class="form-group">
                    <?= $form->field($model, 'email')->textInput(['autofocus' => true ,'class' => "form-control validate"]) ?>
                  </div>
                  <div class="form-group mt-3">
                    <?= $form->field($model, 'password')->passwordInput(['class' => "form-control validate"]) ?>
                  </div>
                  <?= $form->field($model, 'rememberMe')->checkbox() ?>
                  <div class="form-group mt-4">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block text-uppercase']) ?>
                  </div>
                 <?php $form = ActiveForm::end(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>