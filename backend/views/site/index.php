<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
       
        <div class="container" style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
            <!-- row -->
            <div class="row tm-content-row" style="height: 100%; overflow-y: scroll;">
               
               
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll" style="height: 100%;">
                        <h2 class="tm-block-title">Фильмнар:</h2>
                        <a href="/add" class="btn btn-primary btn-block text-uppercase mb-3">Фильм өстәү</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Исеме</th>
                                    <th scope="col">Бүлек</th>
                                    <th scope="col">Карады</th>
                                    <th scope="col">Вакыт</th>
                                    <th scope="col">Яңа</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($articles as $article):?>
                                <tr>
                                    <th scope="row"><b><a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>">#<?= $article->id ?></a></b></th>
                                    <td><b><?= $article->title ?></b></td>
                                    <td><b><?= $article->category->title ?></b></td>
                                    <td><b><?= $article->viewed ?></b></td>
                                    <td><b><?= $article->date ?></b></td>
                                    <td>
                                        <?php if($article->new === 1): ?>
                                            <div class="tm-status-circle moving">
                                        <?php else: ?>
                                            <div class="tm-status-circle cancelled">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($article->status === 1): ?>
                                            <div class="tm-status-circle moving">
                                        <?php else: ?>
                                            <div class="tm-status-circle cancelled">
                                    <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= Url::to(['site/delete', 'id' => $article->id]) ?>" onclick="return confirm('Вы уверены?')" class="tm-product-delete-link">
                                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                                        </a>
                                    </td>
                                </tr>   
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>