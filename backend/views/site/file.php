<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'My Yii Application';
$i = 1; 
?>
       
        <div class="container" style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
            <!-- row -->
            <div class="row tm-content-row" style="height: 100%; overflow-y: scroll;">
               
               
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll" style="height: 100%;">
                        <h2 class="tm-block-title">Файллар:</h2>
                        <a href="<?= Url::toRoute(['site/add-file', 'id' => $article->id]); ?>" class="btn btn-primary btn-block text-uppercase mb-3">Файл өстәү</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Исеме</th>
                                    <th scope="col">Путь</th>
                                    <th scope="col">Вакыт</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php foreach ($files as $file):?>
                                    
                                <tr>
                                    <th scope="row"><b><a href="<?= Url::toRoute(['site/file', 'id' => $file->id]); ?>">#<?= $i ?></a></b></th>
                                    <td><b><?= $file->title ?></b></td>
                                    <td><b><?= $file->file ?></b></td>
                                    <td><b><?= $file->date ?></b></td>
                                    <td>
                                        <?php if($file->status === 1): ?>
                                            <div class="tm-status-circle moving">
                                        <?php else: ?>
                                            <div class="tm-status-circle cancelled">
                                    <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= Url::to(['site/delete-file', 'id' => $file->id]) ?>" onclick="return confirm('Вы уверены?')" class="tm-product-delete-link">
                                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                <?php $i++; ?>   
                             <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>