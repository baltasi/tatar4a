 <?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
 use common\models\Article;
?>
<div class="container tm-mt-big tm-mb-big"  style="height: 100%;margin-top: 0px;padding-top: 115px;padding-bottom: 115px;">
      <div class="row" style="height: 100%; overflow-y: scroll;">
        <div class="col-xl-9 col-lg-10 col-md-12 col-sm-12 mx-auto">
          <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
            <div class="row">
              <div class="col-12">
                <h2 class="tm-block-title d-inline-block">Add file</h2>
              </div>
            </div>
            <div class="row tm-edit-product-row">
              <div class="col-xl-6 col-lg-6 col-md-12">
                   	<?php $form = ActiveForm::begin(['options' => ['class' => 'tm-edit-product-form']]); ?>
                  <div class="form-group mb-3">
                    <?= $form->field( $model, 'title')->textInput( ['class' => "form-control validate"]) ?>
                  </div>
                  <div class="form-group mb-3">
                    <?= $form->field( $model, 'file')->textarea( ['class' => "form-control validate tm-small", 'rows'=> 5 ]) ?>
                  </div>
              </div>
               <div class="form-group mb-3 col-xs-12 col-sm-6">
                          <?= $form->field( $model, 'status' )->dropDownList(Article::getStatusList(),['prompt' => 'Сайланмаган','class' => 'custom-select tm-select-accounts']); ?>
                        </div>
                <div class="col-12">
                <?= Html::submitButton( 'өстәргә', ['class' => 'btn btn-primary btn-block text-uppercase'] ) ?>
              </div>
             <?php $form = ActiveForm::end(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>