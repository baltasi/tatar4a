<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/roboto.css',
        'css/fontawesome.min.css',
        'css/bootstrap.min.css',
        'css/templatemo-style.css',
        'jquery-ui-datepicker/jquery-ui.min.css',
        

    ];
    public $js = [
        'js/jquery-3.3.1.min.js',
        'js/bootstrap.min.js',
        'js/moment.min.js',
        'js/Chart.min.js',
        'js/tooplate-scripts.js',
        'jquery-ui-datepicker/jquery-ui.min.js',
        'js/filereader.js',

    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
