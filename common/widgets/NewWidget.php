<?php

namespace common\widgets;

use yii\base\Widget;
use common\models\Article;
use Yii;

class NewWidget extends Widget{

	public function run(){
		$news = Article::find()->where(['status' => 1])->orderBy('id DESC')->limit(5)->all();

	return $this->render('new', compact('news'));
	}
}