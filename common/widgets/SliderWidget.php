<?php

namespace common\widgets;

use yii\base\Widget;
use common\models\Article;
use Yii;
use yii\db\Expression;

class SliderWidget extends Widget{

	public function run(){

		$cache = Yii::$app->cache->get('rand');
		if(!$cache){
			$rand = Article::find()->where(['>=','id', 102])->orderBy(new Expression('rand()'))->one();
			$cache = $this->render('slider', compact('rand'));
			Yii::$app->cache->set('rand',$cache, 60*12);
		}
		return $cache;
	}
}