<?php

use yii\helpers\Url;
?>
<div class="slider-container" style="height: 558px;">
    <div class="slider-control left inactive"></div>
    <div class="slider-control right"></div>
    <div class="slider" >
        <div class="slide slide-0 active" >
            <div class="slide__bg"  >
            </div>
            <div class="slide__content" style="">
                <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
                    <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
                </svg>
                <div class="slide__text" >
                    <h2 class="slide__text-heading"><?= $rand['title']?></h2>
                    <p class="slide__text-desc"><?= $rand['text']?></p>
                    <a class="slide__text-link" href="<?= Url::toRoute(['site/video', 'id' => $rand['id']]); ?>">Тулысынча</a>
                </div>
            </div>
        </div>
    </div>
</div