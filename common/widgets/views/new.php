<?php

use yii\helpers\Url;
?>

<div class="banner-bottom">
        <div class="container" style="padding-top: 2%;">
            <div class="w3_agile_banner_bottom_grid">
                <div id="owl-demo" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
                	<div class="container">
                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style=" left: 0px; display: block;">
<?php foreach ($news as $new):?>
                    	<div class="owl-item" style="width: 228px;"><div class="item">
                        <div class="w3l-movie-gride-agile w3l-movie-gride-agile1">
                            <a href="<?= Url::toRoute(['site/video', 'id' => $new['id']]); ?>" class="hvr-shutter-out-horizontal"><img src="<?= $new->getImage(); ?>" title="album-name" class="img-responsive" alt=" ">
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="single.html"><?=$new['title']?></a></h6>                          
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><i class="fa fa-clock"></i> <?=$new['film_date']?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <li><a class="s-facebook"><i class="fa fa-eye"></i></a></li> <?= $new['viewed']; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                           <?php if($new['new'] == 1):?>
                                <div class="ribben">
                                    <p>NEW</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div></div>
<?php endforeach;?>
                   </div></div>
               </div>
               <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
            </div>          
        </div>
    </div>