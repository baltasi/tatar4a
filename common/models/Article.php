<?php

namespace common\models;


use common\models\Comment;
use common\models\User;
use common\models\Category;
use Yii;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $text 
 * @property string $date
 * @property string $image
 * @property string $image_icon
 * @property int $viewed
 * @property int $user_id
 * @property int $status
 * @property int $category_id
 * @property int $film_date
 * @property int $new
 *
 * @property Category $category
 * @property User1 $user
 * @property Comment[] $comments
 * @property File[] $files
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text','title','status','category_id','film_date','new'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['viewed', 'user_id', 'status', 'category_id', 'film_date', 'new'], 'integer'],
            [['title', 'image', 'image_icon'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
/*
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
*/
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'title' => 'Исеме',
            'text' => 'Фильм турында',
            'date' => 'Date',
            'image' => 'Аватар',
            'image_icon' => 'Обой',
            'viewed' => 'Карады',
            'user_id' => 'User ID',
            'status' => 'Чыгарырга',
            'category_id' => 'Бүлек',
            'film_date' => 'Фильм чыкты(ел)',
            'new' => 'Яңа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['article_id' => 'id']);
    }

    public function getImage(){
        return ($this->image) ? '/images/'.$this->image : '../no-image.jpeg';
    }

    public function getImageIcon()
    {

        return ($this->image_icon) ? '/images-icon/' . $this->image_icon : '../no-image.png';
    }

    public function getArticleComments(){
        return $this->getComments()->where(['status' => 1])->orderBy('id desc')->all();
    }

    public static function getStatusList(){
        return [0 => 'Юк', 1 => 'Әйе' ];
    }

    public static function getNewList(){
        return [0 => 'Юк', 1 => 'Әйе' ];
    }
/*
    public static function getCategoryList(){
        return [1 => 'Кино', 2 => 'Сериал', 3 => 'Мультфильм'];
    }
  */ 

  public function saveImage($filename){
    $this->image = $filename;
    return $this->save();
  } 

  public function saveImageIcon($fileicon){
    $this->image_icon = $fileicon;
    return $this->save();
    } 

}
