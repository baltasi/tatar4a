<?php

namespace frontend\controllers;

use frontend\models\ContactForm;
use Yii;

//$agent = strtolower($_SERVER['HTTP_USER_AGENT']);

class ContactController extends \yii\web\Controller
{


    public function actionIndex()
    {
        $model = new ContactForm();

        if ($model->load( Yii::$app->request->post() )) {
            /*$agent = $this->agent();
            if($this->check_mobile_device($agent)){
                $model->check_mobile_device = $agent.'/mobile';
            }else{
                $model->check_mobile_device = $agent.'/PC';
            }
            */


            if (!Yii::$app->user->isGuest) {

                $model->name = Yii::$app->user->identity->username;
                $model->email = Yii::$app->user->identity->email;

                if ($model->validate()) {

                    if ($model->save(false)) {
                        Yii::$app->session->setFlash( 'success', 'Сезнен хатыгыз уңышлы гына җибәрелде' );
                        return $this->refresh();

                    } else {
                        Yii::$app->session->setFlash( 'error', 'Хата' );
                    }
                } else {
                    Yii::$app->session->setFlash( 'error', 'Хата' );
                }
            } else {
                if ($model->validate()) {
                    if ($model->save(false )) {

                        Yii::$app->session->setFlash( 'success', 'Сезнен хатыгыз уңышлы гына җибәрелде' );
                        return $this->refresh();

                    } else {
                        Yii::$app->session->setFlash( 'error', 'Хата' );
                    }
                } else {
                    Yii::$app->session->setFlash( 'error', 'Хата' );
                }
            }


        }

        return $this->render( 'index', [
            'model' => $model,
        ] );
    }

    public function check_mobile_device($agent) { 
    $mobile_agent_array = array('ipad', 'iphone', 'android', 'pocket', 'palm', 'windows ce', 'windowsce', 'cellphone', 'opera mobi', 'ipod', 'small', 'sharp', 'sonyericsson', 'symbian', 'opera mini', 'nokia', 'htc_', 'samsung', 'motorola', 'smartphone', 'blackberry', 'playstation portable', 'tablet browser');    
    foreach ($mobile_agent_array as $value) {    
        if (strpos($agent, $value) !== false) return true;   
    }       
    return false; 
    }

    public function agent(){
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        return $agent;
    }

}
