<?php

namespace frontend\controllers;

use common\models\Article;
use frontend\models\SearchForm;
use Yii;

class SearchController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new SearchForm();


        if($search = Yii::$app->request->get('search')){

            $model->date = date('Y-m-d');
            $model->text = $search;
            $model->ip = Yii::$app->request->userIP;
            if($model->save()){
                $query = Article::find()->where(['like', 'title', $search])->all();
            }

        }


        $id = 0;

        return $this->render('index',[
            'query' => $query,
            'search'=> $search,
            'id' => $id
        ]);
    }

}
