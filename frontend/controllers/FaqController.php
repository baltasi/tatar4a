<?php

namespace frontend\controllers;

use common\models\Faq;

class FaqController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $faq = Faq::find()->all();
        return $this->render('index',[
            'faq' => $faq,
        ]);
    }

}
