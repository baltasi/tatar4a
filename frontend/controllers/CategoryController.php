<?php

namespace frontend\controllers;

use common\models\Article;
use common\models\Category;
use yii\data\Pagination;
use yii\web\HttpException;

class CategoryController extends \yii\web\Controller
{
    public function actionIndex($nick)
    {
        $category = Category::findOne(['nick' => $nick]);

        if (empty( $category )) {
            throw new HttpException( 404, 'Сез эзләгән '.$nick.' бите табылмады' );
        } else {

            $id = $category->id;
            $article = Article::find()->where(['category_id' => $id])->orderBy( 'date DESC' );

            $count = $article->andWhere(['status' => 1])->count();

            $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 36]);

            $articles = $article->offset($pagination->offset)->limit($pagination->limit)->all();
        }
        return $this->render('index',[
            'category' => $category,
            'articles' => $articles,
            'pagination' => $pagination


        ]);
    }




}
