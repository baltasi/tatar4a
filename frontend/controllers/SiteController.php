<?php

namespace frontend\controllers;

use app\models\CommentForm;
use common\models\Article;
use common\models\Raits;

use common\models\Rating;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\File;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm; 
use frontend\models\ContactForm;
use yii\web\HttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {


        $kino = Article::find()->where( ['category_id' => 1] )->andWhere( ['status' => 1] )->orderBy( 'date DESC' )->limit( 12 )->all();
        $serial = Article::find()->where( ['category_id' => 2] )->andWhere( ['status' => 1] )->orderBy( 'date DESC' )->limit( 12 )->all();
        $animacionnij = Article::find()->where( ['category_id' => 3] )->andWhere( ['status' => 1] )->orderBy( 'date DESC' )->limit( 12 )->all();

        return $this->render( 'index', [
            'kino' => $kino,
            'serial' => $serial,
            'animacionnij' => $animacionnij,
        ] );
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load( Yii::$app->request->post() ) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render( 'login', [
                'model' => $model,
            ] );
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load( Yii::$app->request->post() )) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login( $user )) {
                    return $this->goHome();
                }
            }
        }

        return $this->render( 'signup', [
            'model' => $model,
        ] );
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load( Yii::$app->request->post() ) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash( 'success', 'Check your email for further instructions.' );

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash( 'error', 'Sorry, we are unable to reset password for the provided email address.' );
            }
        }

        return $this->render( 'requestPasswordResetToken', [
            'model' => $model,
        ] );
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm( $token );
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException( $e->getMessage() );
        }

        if ($model->load( Yii::$app->request->post() ) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash( 'success', 'New password saved.' );

            return $this->goHome();
        }

        return $this->render( 'resetPassword', [
            'model' => $model,
        ] );
    }


    public function actionVideo($id)
    {


        $article = Article::findOne( $id );

        if (empty( $article )) {
            throw new HttpException( 404, 'Сез эзләгән фильм табылмады.' );
        } else {


            $comments = $article->getArticleComments();
            $commentForm = new CommentForm();

            $files = File::find()->where(['article_id' => $id])->andWhere(['status' => 1])->all();

            $article->viewed += 1;
            $article->save( false );

            //выбираем все голоса по данной записи
            $rating = Rating::find()->where( ['article_id' => $id] )->all();
            if (count( $rating ) > 0) {
                $allRaits = Rating::find()->where( ['article_id' => $id] )->select( 'rating' );
                $allUsers = $allRaits->count();//сумма всех учетных записей пользователей к дан. материалу (1а запись - 1 пользователь)
                $sumVotes = $allRaits->sum( 'rating' );//сумма всех оценок пользователей к дан. материалу
                $totalRating = round( $sumVotes / $allUsers, 2 );// округляем до сотых
            } else $totalRating = 0;

        }


        return $this->render( 'video', [
            'article' => $article,
            'comments' => $comments,
            'commentForm' => $commentForm,
            'rating' => $rating,
            'sum' => $totalRating,
            'files' => $files,

        ] );
    }

    public function actionStars(){
        $model = new Rating();

        $user  = Rating::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['article_id' => $_GET['id']])->all();
        if(!$user){
            $model->rating = $_GET['rating'];
            $model->article_id = $_GET['id'];
            $model->user_id = $_GET['idUser'];
            if($model->save()){
                Yii::$app->session->setFlash( 'stars', 'Сезнен тавыш уңышлы гына җибәрелде' );
                return true;
            }
        }else {
            Yii::$app->session->setFlash( 'stars', 'Сезнен инде тавыш бирдегез.' );
            return true;
        }

        return false;
    }

     public function actionDownload($id){
        $data = File::findOne($id);
        header("Cache-Control: public");
        header("Pragma: public");
        header("Content-Type: ".pathinfo($data->file, PATHINFO_EXTENSION));
        header("Content-Type: video/mp4");
        header("Content-Disposition: attachment; filename=".$data->title .".mp4;");
        header("Content-Length: ".filesize($data->file));
        header("Content-Transfer-Encoding: binary");
            return readfile($data->file);
    }


    public function actionComment($id)
    {

        $model = new CommentForm();

        if (Yii::$app->request->isPost) {
            $model->load( Yii::$app->request->post() );
            if ($model->saveComment( $id )) {
                Yii::$app->getSession()->setFlash( 'success', 'Сезнен фикер остэлде' );
                return $this->redirect( ['site/video', 'id' => $id] );
            }
        }
    }
}
