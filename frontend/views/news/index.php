<?php
/* @var $this yii\web\View */


$this->title = 'Яңалыклар';

$this->registerMetaTag(['name' => 'keywords', 'content' =>  'Яңалыклар, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);
$this->registerMetaTag(['name' => 'description', 'content' =>  'Яңалыклар, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);

?>
<div class="faq">
    <div class="container">
        <h3 class="head">Яңалыклар</h3>
        <div class="grid_3 grid_4 w3layouts">
            <div class="contact-agile">
                
                 <div class="grid_3 grid_5 agile">
                    <h4>Дусларынга cөйләү - <i class="fa fa-clock" aria-hidden="true"></i> 11.07.2019</h4>
                    <div class="well">Cайтыбызда фильнарны дусларынга cөйләп була </div>
                </div>

                <div class="grid_3 grid_5 agile">
                    <h4>Рейтинг - <i class="fa fa-clock" aria-hidden="true"></i> 29.03.2019</h4>
                    <div class="well">Cайтыбызда фильмнарга тавыш калдырып була.</div>
                </div>

                <div class="grid_3 grid_5 agile">
                    <h4>Сайт эшли башлады - <i class="fa fa-clock" aria-hidden="true"></i> 24.10.2018</h4>
                    <div class="well">Бүгеннән башлап безнең сайтыбыз бар көченә эшли башлады</div>


                </div>

            </div>
        </div>
    </div>
</div>