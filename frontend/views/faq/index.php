<?php
/* @var $this yii\web\View */

$this->title = 'Сорау-жавап';

$this->registerMetaTag(['name' => 'keywords', 'content' =>  'Сорау-жавап, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);
$this->registerMetaTag(['name' => 'description', 'content' =>  'Сорау-жавап, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);

?>
<div class="faq">
    <div class="container">
        <h3 class="head">Сорау-жавап</h3>
        <div class="grid_3 grid_4 w3layouts">
            <div class="contact-agile">
                <div class="panel-group w3l_panel_group_faq" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach ($faq as $article): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingEight">
                                <h4 class="panel-title asd">
                                    <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $article->id ?>" aria-expanded="false" aria-controls="collapse<?= $article->id ?>">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i><?= $article->vopros ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?= $article->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $article->id ?>">
                                <div class="panel-body panel_text"><?= $article->otvet ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>


