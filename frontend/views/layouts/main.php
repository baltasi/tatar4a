<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 23.11.18
 * Time: 23:58
 */


use frontend\assets\PublicAsset;
use frontend\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google-site-verification" content="NOnarpLmgddYBgW7Do0GnO99H2u3pU4qb6TEAFyMzkM" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="397404781e7a18b3" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?> | Татарча фильмнар</title>
        <?php $this->head() ?>
         <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <script>
            $(document).ready(function() {
                $("#owl-demo").owlCarousel({

                    autoPlay: 3000, //Set AutoPlay to 3 seconds

                    items : 5,
                    itemsDesktop : [640,4],
                    itemsDesktopSmall : [414,3]

                });
            });
        </script>
        <!-- //banner-bottom-plugin -->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
        <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
    </head>
    <body>
    <?php $this->beginBody() ?>


    <!-- header -->
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter50889239 = new Ya.Metrika2({
                        id:50889239,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true });
                } catch(e) { }
            });
            var
                n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            }
            else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/50889239" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <div class="header">
        <div class="container">
            <div class="w3layouts_logo">
                <a href="/"><h1>Tatar4а <span>фильмнар</span></h1></a>
            </div>
            <div class="w3_search">
                <form action="<?= Url::toRoute(['search/index']) ?>" method="get">
                    <input type="text" name="search" placeholder="Сүз кертегез" required="">
                    <input type="submit" value="Эзләү">
                </form>
            </div>

            <div class="w3l_sign_in_register">
                <ul>
                    <?php if(Yii::$app->user->isGuest): ?>
                        <li><a href="/login">Авторизация</a></li>
                        <li><a href="/signup">Теркәлү</a></li>
                    <?php else: ?>
                        <?= Html::beginForm(['/auth/logout'], 'post')
                        . Html::submitButton(
                            'Чыгу (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout', 'style'=>"padding-top:10px;"]
                        )
                        . Html::endForm() ?>

                    <?php endif; ?>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //header -->

   


    <div class="movies_nav">
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <?php
                    NavBar::begin();
                    echo Nav::widget([
                        'options' => ['class' => 'nav navbar-nav'], // стили ul
                        'items' => [
                            ['label' => 'Баш бит', 'url' => ['/site/index']],
                            ['label' => 'Яңалыклар', 'url' => ['/news/index']],
                            ['label' => 'Өстәү', 'url' => ['/add/index']],
                            ['label' => 'Сорау-жавап', 'url' => ['/faq/index']],
                            ['label' => 'Конкурс', 'url' => ['#']],
                            ['label' => 'Сайт турында', 'url' => ['/about/index']],
                            ['label' => 'Элемтә', 'url' => ['/contact/index']],
                        ],
                        'encodeLabels' =>false,
                    ]);
                    NavBar::end(); // закрываем виджет

                    ?>


                </div>
            </nav>
        </div>
    </div>
    <!-- //nav -->
    

    <?= $content ?>

    <!-- footer -->
    <div class="footer">
        <div class="container">
            <div class="w3ls_footer_grid">
                <div class="col-md-6 w3ls_footer_grid_left">
                    <div class="w3ls_footer_grid_left1">
                        <h2>Безгә языл дустым.</h2>
                        <div class="w3ls_footer_grid_left1_pos">
                            <form action="#" method="post">
                                <input type="email" name="email" placeholder="Әлегә 'язылу' эшләми " required="">
                                <input type="submit" value="Send">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 w3ls_footer_grid_right">
                    <a href="index.html"><h2>Tatar4а<span>фильмнар</span></h2></a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-5 w3ls_footer_grid1_left">
                <p>&copy; 2018 - <?= date("Y"); ?> Бары тик татарча кинолар, сериаллар һәм мультфильмнар.</p>
            </div>
            <div class="col-md-7 w3ls_footer_grid1_right">
                <ul>
                    <li>
                        <a href="#">Конкурс</a>
                    </li>
                    <li>
                        <a href="/news">Яңалыклар</a>
                    </li>
                    <li>
                        <a href="#">Шикаять</a>
                    </li>
                    <li>
                        <a href="/contact">Элемтә</a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //footer -->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".dropdown").hover(
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                    $(this).toggleClass('open');
                }
            );
        });
    </script>
    <!-- //Bootstrap Core JavaScript -->
    <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */

            $().UItoTop({ easingType: 'easeOutQuart' });

        });
    </script>
    <!-- //here ends scrolling icon -->


    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>