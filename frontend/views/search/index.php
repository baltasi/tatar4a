<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Эзләү:'. $search ;

?>
<div class="faq">
    <h4 class="latest-text w3_faq_latest_text w3_latest_text">Эзләү</h4>
    <div class="container">
        <div class="agileits-news-top">
            <ol class="breadcrumb">
                <li><a href="/">Баш бит</a></li>
                <li class="active">Эзләү:<?= $search ?></li>
            </ol>
        </div>
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <div class="agile-news-table">
                <?php if (!empty($query)): ?>
                <table id="table-breakpoint5">
                    <thead>
                    <tr>
                        <th>№:</th>
                        <th>Исем:</th>
                        <th>Бүлек:</th>
                        <th>Ел</th>
                        <th>Карады:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($query as $search): ?>
                        <tr>
                            <td><?php $id++; echo $id ?></td>
                            <td class="w3-list-img"><a href="<?= Url::to(['site/video', 'id' => $search->id]) ?>"> <span> <?= $search->title ?> </span></a></td>
                            <td><?= $search->category->title ?></td>
                            <td><?= $search->film_date ?></td>
                            <td><?= $search->viewed ?> кулланучы </a></td>
                        </tr>
                    <?php endforeach;?>
                    <?php else: ?>
                        <div class="alert alert-warning" role="alert">Сез керткән сүз буенча бер ни дә табылмады</div>
                    <?php endif; ?>
                </table >
            </div >
        </div >

    </div >
</div >
</div >
