<?php
/* @var $this yii\web\View */


use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->registerMetaTag(['name' => 'keywords', 'content' =>  $category->title .', бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);
$this->registerMetaTag(['name' => 'description', 'content' =>  $category->title .', бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);

$this->title = $category->title;
?>
<!-- /w3l-medile-movies-grids -->
<div class="general-agileits-w3l">
    <div class="w3l-medile-movies-grids">

        <!-- /movie-browse-agile -->

        <div class="movie-browse-agile">
            <!--/browse-agile-w3ls -->
            <div class="browse-agile-w3ls general-w3ls">
                <div class="container">
                    <h3 class="head"><?= $category->title ?></h3>
                    <div class="grid_3 grid_4 w3layouts">
                        <div class="contact-agile">
                            <?php foreach ($articles as $article): ?>
                                <div class="col-md-2 w3l-movie-gride-agile" style="height: 350px;">
                                    <a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>" class="hvr-shutter-out-horizontal"><img src="<?= $article->getImage(); ?>" title="<?= $article->title ?>" alt="" style="width: 177px;height: 265px;"/>
                                        <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                                    </a>
                                    <div class="mid-1">
                                        <div class="w3l-movie-text">
                                            <h6><a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>"><?= $article->title ?></a></h6>
                                        </div>
                                        <div class="mid-2">

                                            <p><?= $article->film_date; ?></p>
                                            <div class="block-stars">
                                                <ul class="w3l-ratings">
                                                    <li><a class="s-facebook"><i class="fa fa-eye"></i></a></li> <?= $article->viewed; ?>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <?php if($article->new == 1):?>
                                        <div class="ribben">
                                            <p>NEW</p>
                                        </div>
                                    <?php endif; ?>
                                </div>

                            <?php endforeach; ?>

                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

                <!--//browse-agile-w3ls -->
                <div class="blog-pagenat-wthree">

                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                    ?>
                </div>
            </div>
            <!-- //movie-browse-agile -->
        </div>
    </div>
</div>
</div>
<!-- //w3l-medile-movies-grids -->
</div>
<!-- //comedy-w3l-agileits -->



