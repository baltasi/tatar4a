<?php
/* @var $this yii\web\View */

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Элемтэ';

$this->registerMetaTag(['name' => 'keywords', 'content' =>  'Элемтә, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);
$this->registerMetaTag(['name' => 'description', 'content' =>  'Элемтә, бары тик саф татарча кинолар,сериаллар,мультфильмнар tatar4a.ru'  ]);

$form = ActiveForm::begin(['options' => ['class' => 'form-horizontal', 'id' => 'contactForm']]);
?>

<!-- contact -->

<div class="faq">
    <div class="container">
        <h3 class="head">Элемтә</h3>
        <div class="grid_3 grid_4 w3layouts">
            <div class="contact-agile">
                <?php if (Yii::$app->session->hasFlash( 'success' )): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span claaria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash( 'success' ); ?>
                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->session->hasFlash( 'error' )): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash( 'error' ); ?>
                    </div>
                <?php endif; ?>
                <?php if(Yii::$app->user->isGuest): ?>
                    <?= $form->field( $model, 'name' ) ?>
                    <?= $form->field( $model, 'email' ) ?>
                <?php else: ?>
                    <div class="form-group field-contactform-first_name required">
                        <label class="control-label" for="contactform-first_name">Исем: <font color="red" ><?= Yii::$app->user->identity->username  ?></font></label>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-contactform-last_name required">
                        <label class="control-label" for="contactform-last_name">Email: <font color="red" ><?= Yii::$app->user->identity->email  ?></font></label>
                        <div class="help-block"></div>
                    </div>

                <?php endif; ?>

                <?= $form->field( $model, 'subject' ) ?>

                <?= $form->field( $model, 'body' )->textarea( ['rows' => 5] ) ?>

                <?=$form->field($model, 'check_box')->checkbox();?>

                <div class="form-group">
                    <?= Html::submitButton( 'Җибәрү', ['class' => 'btn btn-primary', 'name' => 'contact-button'] ) ?>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- //contact -->


<?php
$form = ActiveForm::end();
?>
