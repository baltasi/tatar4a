<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="faq">
    <div class="container">
        <h3 class="head"><?= Html::encode($this->title) ?></h3>
        <p class="lead"><?= nl2br(Html::encode($message)) ?></p
    </div>
</div>
