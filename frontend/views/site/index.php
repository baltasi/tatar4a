<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use common\widgets\NewWidget;
use common\widgets\SliderWidget;

$this->title = 'Баш бит';
$this->registerMetaTag(['name' => 'description', 'content' =>  'Татарча фильмнар, татарча кинолар, татрча сериаллар  һәм татарча мультфильмнар'  ]);



?>

<?= SliderWidget::widget() ?>

<?= NewWidget::widget() ?>

<!-- Кино -->
<div class="general">
    <h4 class="latest-text w3_latest_text"><a href="/category/kino">Кино</a></h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <div id="myTabContent" class="tab-content">
                <div class="w3_agile_featured_movies">
                    <?php foreach ($kino as $article):?>
                        <div class="col-md-2 w3l-movie-gride-agile"  style="height: 350px;">
                            <a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>" class="hvr-shutter-out-horizontal"><img src="<?= $article->getImage(); ?>" title="<?= $article->title ?>" class="img-responsive" alt=" " />
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>"><?= $article->title ?></a></h6>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><i class="fa fa-clock"></i> <?= $article->film_date; ?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <li><a class="s-facebook"><i class="fa fa-eye"></i></a></li> <?= $article->viewed; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                             <?php if($article->new == 1):?>
                                <div class="ribben">
                                    <p>NEW</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Сериал -->
<div class="general">
    <h4 class="latest-text w3_latest_text"><a href="/category/serial">Сериал</a></h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <div id="myTabContent" class="tab-content">
                <div class="w3_agile_featured_movies">
                    <?php foreach ($serial as $article):?>
                        <div class="col-md-2 w3l-movie-gride-agile"  style="height: 350px;">
                            <a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>" class="hvr-shutter-out-horizontal"><img src="<?= $article->getImage(); ?>" title="<?= $article->title ?>" class="img-responsive" alt=" " />
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>"><?= $article->title ?></a></h6>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><i class="fa fa-clock"></i> <?= $article->film_date; ?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <li><a class="s-facebook"><i class="fa fa-eye"></i></a></li> <?= $article->viewed; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <?php if($article->new == 1):?>
                                <div class="ribben">
                                    <p>NEW</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Мультфильм -->
<div class="general">
    <h4 class="latest-text w3_latest_text"><a href="/category/animacionnij">Мультфильм</a></h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <div id="myTabContent" class="tab-content">
                <div class="w3_agile_featured_movies">
                    <?php foreach ($animacionnij as $article):?>
                        <div class="col-md-2 w3l-movie-gride-agile"  style="height: 350px;">
                            <a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>" class="hvr-shutter-out-horizontal"><img src="<?= $article->getImage(); ?>" title="<?= $article->title ?>" class="img-responsive" alt=" " />
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="<?= Url::toRoute(['site/video', 'id' => $article->id]); ?>"><?= $article->title ?></a></h6>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><i class="fa fa-clock"></i> <?= $article->film_date; ?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <li><a class="s-facebook"><i class="fa fa-eye"></i></a></li> <?= $article->viewed; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <?php if($article->new == 1):?>
                                <div class="ribben">
                                    <p>NEW</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //general -->