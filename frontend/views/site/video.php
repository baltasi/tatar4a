<?php


use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\widgets\NewWidget;

if(!Yii::$app->user->isGuest){
    $id = Yii::$app->user->identity->id;
}else{
    $id = 0;
}

//$this->registerMetaTag(['name' => 'keywords', 'content' =>  'Татарча фильм Татрча кино Татарча сериал Татарча мультфильм'  ]);
$this->registerMetaTag(['name' => 'description' , 'content' => $article->title .' Татарча фильм Татарча '. mb_strtolower($article->category->title)]);

$this->registerMetaTag(['name' => 'twitter:title' , 'content' => $article->title]);
$this->registerMetaTag(['name' => 'twitter:description' , 'content' => $article->title]);
$this->registerMetaTag(['name' => 'og:url' , 'content' => 'tatar4a.ru']);

$this->registerMetaTag(['property' => 'og:title' , 'content' => $article->title]);
$this->registerMetaTag(['property' => 'og:description' , 'content' => $article->title]);
$this->registerMetaTag(['property' => 'og:type' , 'content' => 'website']);
$this->registerMetaTag(['property' => 'og:url' , 'content' => 'tatar4a.ru']);



$this->title = $article->title;

?>

<!-- single -->
<div class="single-page-agile-main">
    <div class="container">
        <div class="single-page-agile-info">
            <div class="agileits-single-top">
                <ol class="breadcrumb">
                    <li><a href="/">Баш бит</a></li>
                    <li><?= $article->category->title ?></li>
                    <li class="active"><?= $article->title ?></li>

                </ol>
            </div>
            <div class="single-page-agile-info">
                <div class="show-top-grids-w3lagile">
                    <div class="col-sm-8 single-left">
                        <div class="song">

                            <div class="video-grid-single-page-agileits">
                                <div data-video="dLmKio67pVQ" id="video"><img src="<?= $article->getImageIcon(); ?>" alt="" class="img-responsive" style=" width: 1173px; height: 600px;"/></div>
                            </div>
                        </div>
                        <div class="w3-agile-about-text">
                            <div><h3>Фильм турында</h3></div>
                            <p><?= $article->text ?>
                            <?php if($article->description === NULL): ?>
                                </p>
                            <?php else: ?>
                                <?= $article->description ?></p>
                            <?php endif; ?>
                        </div>
                    <table id="table-breakpoint5">
                        <thead>
                        <tr>
                            <th>Исем:</th>
                            <th>Йөкләү:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty( $files )): ?>
                            <?php foreach ($files as $file): ?>
                                <tr>
                                    <td><?= $file->title ?></td>
                                    <td><a href="<?= $file->file  ?>" target="_blank"><button type="button" class="btn btn-primary">Яндекс диск</button></a></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                                
                            </tr>
                        <?php endif; ?>

                    </table>
                    <div style="max-width: 1280px; margin: 0 auto;  text-align: center;">
                        <div style="padding-top: 4%;padding: 1em 1em;box-shadow: 0px 0 10px #C1C1C1 !important;margin: 1em 0 0 0;">
                   <h1>Рейтинг</h1>
                        <?= kartik\rating\StarRating::widget([
                            'name' => 'rating',
                            'value' => $sum,
                            'pluginOptions' => [
                                //'readonly' => true,
                                'showClear' => false,
                                'showCaption' => false,
                                'disabled' => Yii::$app->user->isGuest ? true : false,//для гостя блокируем кнопки
                                'stars' => 10,
                                'min' => 0,
                                'max' => 10,
                                'step' => 0.5,
                                'size' => 'lg',
                                'defaultCaption' => 'оценка {rating}',
                                'starCaptions' => [
                                    0 => 'Extremely Poor',
                                    1 => 'оценка 1',
                                    2 => 'оценка 2',
                                    3 => 'оценка 3',
                                    4 => 'оценка 4',
                                    5 => 'оценка 5',
                                    6 => 'оценка 6',
                                    7 => 'оценка 7',
                                    8 => 'оценка 8',
                                    9 => 'оценка 9',
                                    10 => 'оценка 10',
                                ],
                            ],
                            'pluginEvents' => [
                                //когда кликаем на звезды всплывает это событие, которое и обробатываем
                                'rating:change' => "function(event, value, caption) {
                        $.ajax({
                         url: '/site/stars',
                                    method:'get',
                                    data: {
                                        rating:value,
                                        id:". $article->id .",
                                        idUser:". $id ." 
                                        },
                                    dataType: 'json',
                                    success:function(data){
                                            location.reload();
                                         }
                                });
                                }"

                            ],
                        ]) ?>
                        <p>Уртача рейтинг - "<?= $sum ?>"  (Тавыш бирде "<?= count($rating) ?>" кеше)</p>
                    </div>
                        <div style="padding-top: 3%;padding: 1em 1em;box-shadow: 0px 0 10px #C1C1C1 !important;margin: 1em 0 0 0;">
        <h1>Дусларынга cөйләү </h1>
        <br><br>
    
        <input value="ВКонтакте" type="button" class="share_btn" data-social="vk"> 
        <input value="Facebook" type="button" class="share_btn" data-social="fb"> 
        <input value="Одноклассники" type="button" class="share_btn" data-social="ok"> 
        <input value="Twitter" type="button" class="share_btn" data-social="tw"> 
        <input value="Google+" type="button" class="share_btn" data-social="gp"> 
    </div>
</div>
                    <div class="clearfix"></div>
                    <div class="all-comments">
                        <div class="all-comments-info">
                            <?php if (!Yii::$app->user->isGuest): ?>
                            <a href="#">Comments</a>
                            <?php if (Yii::$app->session->getFlash( 'success' )): ?>
                                <div class="alert alert-success" role="alert">
                                    <?= Yii::$app->session->getFlash( 'success' ); ?>
                                </div>
                            <?php endif; ?>
                            <div class="agile-info-wthree-box">
                                <?php $form = \yii\widgets\ActiveForm::begin( [
                                    'action' => ['site/comment', 'id' => $article->id],
                                ] ); ?>

                                <?= $form->field( $commentForm, 'comment' )->textarea( ['placeholder' => 'Message'] )->label( false ) ?>
                                <?= Html::submitButton( 'Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button'] ) ?>
                                <div class="clearfix"></div>

                                <?php \yii\widgets\ActiveForm::end(); ?>

                            </div>
                        </div>
                        <div class="media-grids">
                            <?php else: ?>
                                <div class="alert alert-success" role="alert">
                                    <p>Бары тик <strong><a href="/auth/login">АВТОРИЗАЦИЯ</a></strong> үткән
                                        кулланучылар гына фикер калдыра ала. </p>
                                </div>
                            <?php endif; ?>
                            <?php if (!empty( $comments )): ?>
                                <h3>Фикерләр(<?= count( $comments ) ?>)</h3>
                                <?php foreach ($comments as $comment): ?>
                                    <div class="media-grids">
                                        <div class="media">

                                            <div class="media-left">

                                                <img src="/images/user.jpg" title="One movies" alt=" "/>

                                            </div>
                                            <div class="media-body">

                                                <p><?= $comment->text; ?></p>
                                                <span>Автор :<a
                                                        href="#"> <?= $comment->user->username; ?> </a><?= $comment->getDate(); ?></span>
                                            </div>
                                        </div>
                                    </div>


                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="alert alert-success" role="alert">
                                    <p>Кызганычка каршы бу бүлектә әлегә фикерләр юк</p>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>
                <?= NewWidget::widget() ?>
            </div>
        </div>
        <!-- //movie-browse-agile -->


    </div>
    <!-- //w3l-latest-movies-grids -->
</div>
</div>
<!-- //w3l-medile-movies-grids -->
