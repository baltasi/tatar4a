<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="faq">
    <div class="container">
        <h3 class="head">Авторизация</h3>
        <div class="grid_3 grid_4 w3layouts">
            <div class="contact-agile">
                <div class="site-login">
                    <?php $form = ActiveForm::begin( ['id' => 'login-form'] ); ?>

                    <?= $form->field( $model, 'email' )->textInput( ['autofocus' => true] ) ?>

                    <?= $form->field( $model, 'password' )->passwordInput() ?>

                    <?= $form->field( $model, 'rememberMe' )->checkbox() ?>

                    <div style="color:#999;margin:1em 0">
                        <?= Html::a( 'Серсүзне оныттыгыз?', ['site/request-password-reset'] ) ?>.
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton( 'Керү', ['class' => 'btn btn-primary', 'name' => 'login-button'] ) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
