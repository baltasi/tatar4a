<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->registerMetaTag( ['name' => 'keywords', 'content' => 'Сорау-жавап, бары тик саф татарча кинолар,сериаллар,мультфильмнар ...'] );
$this->registerMetaTag( ['name' => 'description', 'content' => 'Сорау-жавап, бары тик саф татарча кинолар,сериаллар,мультфильмнар ...'] );

$this->title = 'Теркәлү';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq">
    <div class="container">
        <h3 class="head">Теркәлү</h3>
        <div class="grid_3 grid_4 w3layouts">
            <div class="contact-agile">

                <div class="site-login">

                    <div class="row">
                        <div class="col-lg-5">
                            <?php $form = ActiveForm::begin( ['id' => 'form-signup'] ); ?>

                            <?= $form->field( $model, 'username' )->textInput( ['autofocus' => true] ) ?>

                            <?= $form->field( $model, 'email' ) ?>

                            <?= $form->field( $model, 'password' )->passwordInput() ?>

                            <?=$form->field($model, 'check_box')->checkbox();?>

                            <div class="form-group">
                                <?= Html::submitButton( 'Теркәлү', ['class' => 'btn btn-primary', 'name' => 'signup-button'] ) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

