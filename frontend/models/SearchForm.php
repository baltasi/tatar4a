<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 26.11.18
 * Time: 20:28
 */

namespace frontend\models;


use yii\db\ActiveRecord;

class SearchForm extends ActiveRecord
{

    public static function tableName()
    {
        return 'search';
    }

}