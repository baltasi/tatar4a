<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 23.11.18
 * Time: 23:54
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class PublicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/style.css',
        'css/contactstyle.css',
        'css/faqstyle.css',
        'css/single.css',
        'css/medile.css',
        'css/jquery.slidey.min.css',
        'css/popuo-box.css',
        'css/font-awesome.min.css',
        'css/owl.carousel.css',
        'list-css/list.css',
        'news-css/news.css',
        'css/font-awesome.min.css',
        'list-css/table-style.css',
        'list-css/basictable.css',

    ];
    public $js = [
      //  'js/jquery-2.1.4.min.js',
        'js/repost.js',
        'js/slider.js',
        'js/owl.carousel.js',
        'text/javascript',
        'js/easing.js',
        //'/js/bootstrap.min.js',
        'js/jquery.slidey.js',
        'js/jquery.dotdotdot.min.js',
        'js/jquery.slidey.js',
        'js/jquery.dotdotdot.min.js',
        'js/move-top.js',
        'js/easing.js',
        'list-js/jquery.basictable.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}